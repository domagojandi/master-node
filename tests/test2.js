/**
 * Created by domagoj on 2/8/17.
 */
const redisCli = require('redis').createClient();
const MasterNode = require('../index.js');
MasterNode.init(redisCli, { driftFactor: 0.01 });
MasterNode.on('master', () => console.log('im master'));
MasterNode.on('masterEnd', () => console.log('im NOT master'));
setInterval(() => {
    require('crypto').createDiffieHellman(100);
}, 1);