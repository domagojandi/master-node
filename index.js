/**
 * Created by domagoj on 2/7/17.
 */
'use strict';
const Redlock = require('redlock');
const Promise = require('bluebird');
const EventEmitter = require('events').EventEmitter;

class MasterNode {
    // TODO: refactor class to set of functions and expose only "isMaster" function
    static init(redisClients, options={}) {
        if (!Array.isArray(redisClients)) {
            redisClients = [redisClients];
        }
        options.masterDuration = options.masterDuration || 10000;
        this._redlock = new Redlock(redisClients, {
            'driftFactor': options.driftFactor,
            //'retryCount': 0
        });
        //this._opts = options;
        if (this._isMaster == null) {
            this._isMaster = false;
        }
        this._masterDuration = options.masterDuration;
        this._events = new EventEmitter();
        this._lastConfirmedMasterTime = 0;
        this._masterCheckingThread = null;
        this._hasThreadWaiting = false;
    }

    static isMaster() {
        return this._tryBecomeMaster();
    }

    static on(event, callback) {
        if (event !== 'master' && event !== 'masterEnd') {
            throw new Error('Event has to be master or masterEnd');
        }
        this._events.on(event, callback);
        if (event === 'master' && this._lastConfirmedMasterTime === 0) {
            this._tryBecomeMaster();
        }
    }

    static _isMasterTimeExpired() {
        return Date.now() - this._lastConfirmedMasterTime > this._masterDuration;
    }

    static _killMasterCheckingThread() {
        if (this._masterCheckingThread) {
            clearInterval(this._masterCheckingThread);
            this._masterCheckingThread = null;
        }
    }

    static _startMasterCheckingThread(lock) {
        if (this._masterCheckingThread) {
            return;  // if thread already running, do nothing
        }
        this._masterCheckingThread = setInterval(() => {
            if (!this._isMasterTimeExpired()) {
                return lock.extend(this._masterDuration).then(() => {
                    this._hasBecameMaster();
                }).catch(() => {
                    console.log("error extending lock");
                    // something went wrong, let timeout expire, i'm not master anymore
                    this._killMasterCheckingThread();
                    this._hasEndedBeingMaster();
                });
            } else {
                console.log("master time expired");
                // master time expired. i'm no longer master node
                this._killMasterCheckingThread();
                this._hasEndedBeingMaster();
            }
        }, this._masterDuration / 2);
    }

    static _hasBecameMaster() {
        if (!this._isMaster) {
            this._events.emit('master');
        }
        this._isMaster = true;
        this._lastConfirmedMasterTime = Date.now();
    }

    static _hasEndedBeingMaster() {
        if (this._isMaster) {
            this._events.emit('masterEnd');
        }
        this._isMaster = false;
        if (!this._hasThreadWaiting) {
            this._hasThreadWaiting = true;
            setTimeout(() => {
                this._hasThreadWaiting = false;
                this._tryBecomeMaster();
            }, this._masterDuration / 2);
        }
    }

    static _tryBecomeMaster() {
        // if master and no need for another check, then return true
        if (this._isMaster && !this._isMasterTimeExpired()) {
            return Promise.resolve(true);
        }
        // if not yet master or time expired, try to become master
        this._killMasterCheckingThread();
        return this._redlock.lock('nodejs:masternode', this._masterDuration).then(lock => {
            this._hasBecameMaster();
            // try to stay the master node
            this._startMasterCheckingThread(lock);
            return true;  // has become master
        }).catch(() => { // TODO: error checking when unable to contact redis
            console.log("Error obtaining lock");
            this._hasEndedBeingMaster();
            return false;  // did not become master
        });
    }
}

module.exports = MasterNode;